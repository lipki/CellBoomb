(function () {
    "use strict";

    var me;
    var main;
    var randWidth;
    var randHeight;
    var randRayon;

    function Cell ( rayon, _main ) {

      main = _main;
      me = this;

      randWidth = d3.randomUniform(main.width);
      randHeight = d3.randomUniform(main.height);

      this.x = randWidth();
      this.y = randHeight();
      this.nx = this.x;
      this.ny = this.y;
      this.r = rayon;
      this.links = [];
      this.neighbor = 0;
      this.boomb = false;
      this.open = false;
      this.flag = false;
      this.polygon = [];
      this.touch = 0;
      this.gNode = 0;

    };

    Cell.prototype = {
      polygonAddData: function ( polygon ) {
        if(polygon) {
          me = this;
          this.touch = 0;
          this.polygon = this.resample( polygon );
          this.polygon.forEach(function(p,i,a) {
            a[i] = [p[0]-me.x,p[1]-me.y];
            if(a[i][0] <= main.strokeWidth || a[i][1] <= main.strokeWidth
            || a[i][0] >= main.width-main.strokeWidth || a[i][1] >= main.height-main.strokeWidth)
              main.touch ++;
          })
        }
      },
      resample: function ( points ) {
        var i = -1,
            n = points.length,
            p0 = points[n - 1], x0 = p0[0], y0 = p0[1], p1, x1, y1,
            points2 = [];
        while (++i < n) {
          p1 = points[i], x1 = p1[0], y1 = p1[1];
          points2.push(
            [(x0 * 2 + x1) / 3, (y0 * 2 + y1) / 3],
            [(x0 + x1 * 2) / 3, (y0 + y1 * 2) / 3],
            p1
          );
          p0 = p1, x0 = x1, y0 = y1;
        }
        return points2;
      },
      nodeAddGFX: function ( gNode ) {

        this.gNode = gNode;
        gNode.append("path").attr("class", "cell");
        gNode.append("circle").attr("class", "center")
             .style("fill", "black")
             //.style("r", function(d) {return d.r});
        gNode.append("text").attr("class", "label")
             .text(function(d) {return ""})
             .style("font-size", (main.rat*main.size.i)+"px")
             .style("transform", "translate(0,"+((main.rat*main.size.i)/3)+"px)");

      },
      updateGFX: function ( node ) {

        this.gNode.attr("transform", function(d){
          //d.x = d.x > d.r ? (d.x < main.width-d.r ? d.x : main.width-d.r) : d.r;
          //d.y = d.y > d.r ? (d.y < main.height-d.r ? d.y : main.height-d.r) : d.r;
          return "translate("+d.x+","+d.y+")"
        })
        .select("path").datum(this.polygon)
          .attr("d", d3.line().curve(d3.curveBasisClosed));

      }
    };

    window.Cell = Cell;
})();
