function PoissonDiskSampling ( dimension, R, K ) {

  this.n = dimension.z == 0 ? dimension.y == 0 ? 1 : 2 : 3;
  var width = dimension.x;
  var height = dimension.y;
  this.K = K;
  this.R = R;
  this.K = K;
  var W = R / Math.sqrt(this.n);
  var grid = undefined;
  var active = [];
  this.result = [];
  var me = this;

  function init () {

    grid = new Grid( Math.floor(width/W), Math.floor(height/W) );

    var rSample = Victor(width, height)
                    .divide(Victor(2,2))
                    .unfloat();
    var gSample = Victor(Math.floor(rSample.x/W), Math.floor(rSample.y/W));
    active.push(rSample);
    grid.add( gSample , rSample );
    me.result.push(rSample);

    poisson();

  }

  function poisson () {

    while ( active.length > 0) {
      var find = findSample()
      if( find ) {
        var rSample = find[0];
        var gSample = find[1];
        if( rSample.x < width && rSample.y < height ) {
          grid.add( gSample , rSample );
          active.unshift(rSample);
          me.result.push(rSample);
        }
      } else active.pop();
    }

  }

  function findSample () {

    var pos = active[active.length-1];

    for ( var k = 0 ; k < me.K ; k++ ) {

      var m = Math.random()*me.R+me.R;
      var rSample = Victor(0, 0)
                      .randomize(Victor(-100, -100), Victor(100, 100))
                      .normalize()
                      .multiply(Victor(m,m))
                      .add(pos)
                      .unfloat();
      var gSample = Victor(Math.floor(rSample.x/W), Math.floor(rSample.y/W));

      if( grid.get( gSample ) == -1 ) {
        if( Attempt(rSample, gSample) )
          return [rSample, gSample];
      }
    }
    return false;

  }

  function Attempt (rSample, gSample) {

    for ( var i = -1 ; i < 2 ; i++ )
    for ( var j = -1 ; j < 2 ; j++ ) {
      var neighbor = gSample.clone().add(Victor(i,j));
      if( grid.get( neighbor ) != undefined && grid.get( neighbor ) != -1 ) {
        var d = rSample.distance(grid.get(neighbor));
        if( d < me.R ) return false;
      }
    }
    return true;

  }

  init();
}

function Grid( width, height, defaut ) {

  this.data = [];
  this.width = width;
  this.height = height;

  this.add = function ( point, input ) {
    if( point.x < 0 || point.x > width || point.y < 0 || point.y > height )
      return undefined;
    this.data[point.x + point.y * this.width] = input;
  }

  this.get = function ( point ) {
    if( point.x < 0 || point.x > width || point.y < 0 || point.y > height )
      return undefined;
    if( this.data[point.x + point.y * this.width] == undefined )
      return -1;
    return this.data[point.x + point.y * this.width];
  }

}
