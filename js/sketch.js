(function () {
    "use strict";

    /*var width = window.innerWidth-5,
        height = window.innerHeight-5,

        CenterSize = 2,
        strokeWidth = 5,
        size = {i:.02,d:.03,a:.04},
        slow = 0,
        nodes = [],
        diagram,

        rat = Math.sqrt((width*width)+(height*height)),//todo parenthése
        nbNodes = Math.floor((width*height)/(((rat*size.d)*(rat*size.d))*Math.PI)/0.9),
        nbBoomb = nbNodes*10/100,
        CellSize = {min:rat*size.i,max:rat*size.a},

        randWidth = d3.randomUniform(width),
        randHeight = d3.randomUniform(height),
        randRayon = d3.randomUniform(CellSize.min, CellSize.max),

        svg = d3.select("svg")
                    .attr("width", width)
                    .attr("height", height),

        gNodes = svg.selectAll("g.node").data(nodes),

        voronoi = d3.voronoi()
                    .extent([[strokeWidth/2, strokeWidth/2], [width-strokeWidth/2, height-strokeWidth/2]])
                    .x(function(d){return d.x})
                    .y(function(d){return d.y}),

        simulation = d3.forceSimulation(nodes)
                       //.force("y", d3.forceY())
                       //.force("x", d3.forceX())
                       //.force("center", d3.forceCenter(width/2, height/2))
                       .force("charge", d3.forceManyBody(-10))
                       .force("collide", d3.forceCollide()
                                           .radius(function(d) {return d.r})
                                           .iterations(2)),

        interval,
        timer,
        openlist = [],
        cells;*/

    var main,
        width = window.innerWidth-5,
        height = window.innerHeight-5,

        padCell = 5,
        nodes = [],
        surface = 0,
        pcBoomb = 10,

        interval,timer,rat,nbNodes,nbBoomb,size,randRayon,

        svg = d3.select("body").append("svg"),

        voronoi = d3.voronoi()
                    .x(function(d){return d.x})
                    .y(function(d){return d.y}),

        simulation = d3.forceSimulation(nodes)
                       .force("charge" , d3.forceManyBody(-10))
                       .force("collide", d3.forceCollide()
                                           .radius(function(d) {return d.r})
                                           .iterations(2)),
        z;


    function Main () {
      main = this;

      this.width = width;
      this.height = height;
      this.size = {i:.02,a:.04};

      rat = Math.sqrt((this.width*this.width)+(this.height*this.height));
      randRayon = d3.randomUniform(rat*this.size.i, rat*this.size.a);
      nbNodes = Math.floor((this.width*this.height), randRayon());

      svg.attr("width", this.width).attr("height", this.height);

      voronoi.extent([[padCell/2, padCell/2], [this.width-padCell/2, this.height-padCell/2]]);

      simulation.force("x", d3.forceX(0))
                .force("y", d3.forceY(0))
                .force("center", d3.forceCenter(width, height))

      //interval = d3.interval(update, 5);
      simulation.on("tick", move);
      timer = d3.timer(watch);
    };

    function update () {

      if( surface < main.width*main.height ) {

        var rayon = randRayon();
        surface += (rayon*2)*(rayon*2);
        nodes.push(new Cell(randRayon(), main));
        simulation.nodes(nodes);

      } else {
        //interval.stop();
        nbNodes = nodes.length;
        nbBoomb = nbNodes*pcBoomb/100;
      }

    };

    function watch () {

      if( simulation.alpha() < simulation.alphaMin() ) {
        //simulation.stop();
        timer.stop();
      }/* else {
        console.log(simulation);
        simulation.tick();

      }*/

    };

    function move () {

      update();
      moveData();
      moveView();

    };

    function moveData () {

      var diagram = voronoi(nodes);
      nodes.forEach(function(p,i,a) {
        p.polygonAddData(diagram.polygons()[i]);
      });

    };

    function moveView () {

      var gNodes = svg.selectAll("g.node").data(nodes);
      var ent = gNodes.enter().append("g") // ENTER
                      .classed("node", true)
                      .each(function(d,j) {
                        nodes[j].nodeAddGFX(d3.select(this))
                      });
          gNodes.merge(ent); // ENTER + UPDATE


          gNodes = svg.selectAll("g.node")
                      .each(function(d,j) {nodes[j].updateGFX(d)});

    };

    Main.prototype = {
      /*watch: function () {
        var main = this;
        if( simulation.alpha() < 0.01 && nodes.length > nbNodes ) {
          simulation.stop();
          timer.stop();

          console.log('stop')

          cells = svg.selectAll("g.node")
                     .filter(function(d, i) {
                       return d.touch < 1?this:null;
                     })
                     .sort(function() {
                       return [true,false][Math.round(d3.randomUniform()())]
                     })
                     .on("click", function() {
                       var me = d3.select(this);
                       me.each(function(d) {
                         if( d.flag )  {
                           openlist.push(me);
                           main.open();
                         } else if( !d.open ) main.flag(me);
                       });
                     });

          var links = diagram.links();
          for (var j = 0, m = links.length; j < m; ++j) {
            var source = cells.filter(function(d, i) {
                      return d.index == links[j].source.index ? this : null
                    });
            var target = cells.filter(function(d, i) {
                      return d.index == links[j].target.index ? this : null
                    });
            if( source.size() && target.size() ) {
              source.each(function(d) { d.links.push(target) });
              target.each(function(d) { d.links.push(source) });
            }
          }

          cells.select("text")
                 .text(function(d) {
                   return d.links.length
                 });

          cells.filter(function(d, i) {
                 return i<nbBoomb?this:null;
               })
               .each(function(d) {
                 d.boomb = true
                 d.links.forEach(function(p,i,a) {
                   p.each(function(d) { d.nbBoomb++ });
                 })
               })
               .classed("boomb", true);

             cells.select("text")
                    .text(function(d) {
                      return d.nbBoomb != 0 ? d.nbBoomb : ''
                    });

        }
      },
      flag: function( cell ) {
        cell.classed("flag", true);
        cell.each(function(d) {d.flag = true});
      },
      open: function() {
        var main = this;
        var cell = openlist.pop();
        cell.classed("open", true);
        cell.classed("flag", false);
        cell.each(function(d) {
          if( !d.boomb && d.nbBoomb == 0 && !d.open ) {
            d.links.forEach(function(p,i,a) {
                openlist.push(p);
            });
          } else if( d.boomb ) {
            cells.classed("open", true);
          }
          d.open = true;
        });

        if( openlist.length > 0 )
          main.open();
      },
      update: function () {

        if( nodes.length <= nbNodes ) {

          nodes.push({x:randWidth(),
                      y:randHeight(),
                      r:randRayon(),
                      links:[],
                      nbBoomb:0,
                      boomb:false,
                      open:false,
                      flag:false});

          simulation.nodes(nodes.slice(1))

          gNodes = svg.selectAll("g.node").data(nodes); // DATA JOIN

          var ent = gNodes.enter() // ENTER
                          .append("g")
                            .classed("node", true)
          ent.append("path").attr("class", "cell");
          ent.append("circle").attr("class", "center");
          ent.append("text").attr("class", "label")
             .text(function(d) {return ""})
             .style("font-size", (rat*size.i)+"px")
             .style("transform", "translate(0,"+((rat*size.i)/3)+"px)")

          gNodes.merge(ent) // ENTER + UPDATE

          simulation = simulation.restart();

        } else interval.stop();

      },
      move : function () {
        var main = this;
        //simulation.tick();

        width = window.innerWidth-5;
        height = window.innerHeight-5;

        diagram = voronoi(nodes);  // rustine

        gNodes = svg.selectAll("g.node")
        gNodes.attr("transform", function(d){
                d.x = d.x > d.r ? (d.x < width-d.r ? d.x : width-d.r) : d.r;
                d.y = d.y > d.r ? (d.y < height-d.r ? d.y : height-d.r) : d.r;
                return "translate("+d.x+","+d.y+")"
              })
              .each(function(d,j) {
                var me = d3.select(this);
                main.polygon( d, diagram.polygons()[j] );
                me.classed("touch",d.touch > 1);
                me.select("path").datum(d.polygon)
                  .attr("d", d3.line().curve(d3.curveBasisClosed));

                //main.link( d, diagram.links()[j] );
              })
              .select("circle")
              .attr("r",function(d) {return d.r});

        //if (elapsed > 20000 && slow ) t.stop();
      },
      polygon:function ( data, sample ) {
        data.touch = 0;
        data.polygon = this.resample(sample);
        data.polygon.forEach(function(p,i,a) {
          a[i] = [p[0]-data.x,p[1]-data.y];
          if(p[0] <= strokeWidth
          || p[1] <= strokeWidth
          || p[0] >= width-strokeWidth
          || p[1] >= height-strokeWidth) data.touch ++;
        })
      },
      link:function ( data, link ) {

      },
      resample : function (points) {
        var i = -1,
            n = points.length,
            p0 = points[n - 1], x0 = p0[0], y0 = p0[1], p1, x1, y1,
            points2 = [];
        while (++i < n) {
          p1 = points[i], x1 = p1[0], y1 = p1[1];
          points2.push(
            [(x0 * 2 + x1) / 3, (y0 * 2 + y1) / 3],
            [(x0 + x1 * 2) / 3, (y0 + y1 * 2) / 3],
            p1
          );
          p0 = p1, x0 = x1, y0 = y1;
        }
        return points2;
      }*/
    };

    window.Main = Main;
})();

new Main();

/**/
